from bottle import run, template, static_file, route, get


@route('/static/<filepath:path>')
def server_static(filepath):
    return static_file(filepath, root='./static')


@get('/')
def get_index():
    return template('hospital')


if __name__ == '__main__':
    run(host='0.0.0.0', port=5800)
